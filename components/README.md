# COMPONENTS

**This directory is not required, you can delete it if you don't want to use it.**

The components directory contains your Vue.js Components.

_Nuxt.js doesn't supercharge these components._


**Understanding the Structure of Component Folder**

1. _each folder in components is associated with pages of the App._

    example- if you find a folder named as homepage in components. it will refer to all the components of homepage only.

2. _if any common component is used, it will be saved in shared folder._


**Static Data**
    All the static data in page is being served by data folder having json files.