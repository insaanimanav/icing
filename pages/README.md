# PAGES

This directory contains your Application Views and Routes.
The framework reads all the `*.vue` files inside this directory and creates the router of your application.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/guide/routing).


**How Components in Pages are being Served**

1. Each Pages has all the sections as its component.
2. component has folders named same as file in pages.